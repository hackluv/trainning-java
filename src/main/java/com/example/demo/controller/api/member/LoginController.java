package com.example.demo.controller.api.member;

import com.example.demo.dto.AccountDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.entity.Message;
import com.example.demo.model.MemberModel;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

public class LoginController extends HttpServlet {
    Gson gson = new Gson();
    HashPassword hash = new HashPassword();
    MemberModel model = new MemberModel();
    Message message = new Message();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");

        // đọc json từ request.
        BufferedReader reader = req.getReader();
        // sử dụng class AccountDTO để hứng
        // dùng thư viện Gson để chuyển đổi đoạn json đọc được tư request thành Object AccountDTO
        Account account = gson.fromJson(reader, Account.class);
        Account account1 = model.login(account.getEmail());
        if (account1 != null){
            Member member = account1.getRefMember().get();

            if (account1.getPassword().equals(hash.md5(account.getPassword() + account1.getSalt()))){
                AccountDTO accountDTO = new AccountDTO(account1, member);
                resp.getWriter().println(gson.toJson(accountDTO));
            }else {
                // tra ve loi
            }
        }else {
            // tra ve loi
        }
    }
}
