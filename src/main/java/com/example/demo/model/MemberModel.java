package com.example.demo.model;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MemberModel {

    public boolean register(Account account, Member member){
        ofy().save().entity(account).now();
        ofy().save().entity(member).now();
        return true;
    }

    public Account login(String email){
        Account account = ofy().load().type(Account.class).filter("email", email).first().now();
        return account;
    }
}
