package com.example.demo.entity;

public class Message {
    private int code; // trả về code ( 200 || 400 || 404 || 500 )
    private String message; // tra ve message

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
