package com.example.demo.util.objectify;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ObjectifyHelper implements ServletContextListener {
    public void contextInitialized(ServletContextEvent event) {
        ObjectifyService.init();
        ObjectifyService.register(Account.class);
        ObjectifyService.register(Member.class);
    }
}
