package com.example.demo.controller.api.member;

import com.example.demo.dto.AccountDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.entity.Message;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.generate.Generate;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;
import com.googlecode.objectify.Ref;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;

public class RegisterController extends HttpServlet {
    Gson gson = new Gson();
    Generate generate = new Generate();
    HashPassword hash = new HashPassword();
    MemberModel model = new MemberModel();
    Message message = new Message();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        // đọc json từ request.
        BufferedReader reader = req.getReader();
        // sử dụng class AccountDTO để hứng
        // dùng thư viện Gson để chuyển đổi đoạn json đọc được tư request thành Object AccountDTO
        AccountDTO accountDTO = gson.fromJson(reader, AccountDTO.class);
        Account account = new Account();
        Member member = new Member();

        String salt = generate.getSalt(10);
        String hashPass = hash.md5(accountDTO.getPassword() + salt);

        account.setId(Calendar.getInstance().getTimeInMillis());
        account.setPassword(hashPass);
        account.setUsername(accountDTO.getUsername());
        account.setSalt(salt);
        account.setEmail(accountDTO.getEmail());
        account.setCreatedAtMLS(Calendar.getInstance().getTimeInMillis());
        account.setUpdatedAtMLS(Calendar.getInstance().getTimeInMillis());
        account.setRole(Account.Role.MEMBER);
        account.setStatus(Account.Status.ACTIVE);
        account.setAvatar("https://i.imgur.com/fCNkJOp.jpg");

        member.setId(Calendar.getInstance().getTimeInMillis());
        member.setFullname(accountDTO.getFullname());
        member.setAddress(accountDTO.getAddress());
        member.setPhone(accountDTO.getPhone());
        int gen = 0;
        switch (accountDTO.getGender()) {
            case "Female":
                gen = 0;
                break;
            case "Male":
                gen = 1;
                break;
            case "Others":
                gen = 2;
                break;
        }
        member.setGender(Member.Gender.findByValue(gen));

        account.setRefMember(Ref.create(member));
        member.setRefAccount(Ref.create(account));

        boolean check = model.register(account, member);
        if (check != true) {
            message.setCode(HttpServletResponse.SC_BAD_REQUEST);
            message.setMessage(ApplicationConstant.REGISTER_FAIL);
            resp.getWriter().println(gson.toJson(message));
        } else {
            message.setCode(HttpServletResponse.SC_OK);
            message.setMessage(ApplicationConstant.REGISTER_SUCCESS);
            resp.getWriter().println(gson.toJson(message));
        }
    }
}
